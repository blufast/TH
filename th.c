/* th.c
 *
 * Copyright (C) 2018 Jason Lethbridge
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "th.h"

unsigned short PORT = 0;
static unsigned short * pArr = NULL;
static unsigned int pit = 0;

#ifdef TH_FEATURE_UPNP
    #include "upnp.h"
#endif

#ifdef USE_NORETURN
static noreturn void sigHandler(int signum)
#else
static void sigHandler(int signum)
#endif
{//Close the program nicely when appropate signals are catched
    printf("\n");
#ifdef TH_FEATURE_UPNP
    if ((FLAG & UPNP) > 0)
    {
        printf("Closing Port... ");
        fflush(stdout);
        int e = IGD_removePort();
        if (e == 0)
            printf("Done\n");
        else
            printf("Error %d\n",e);

        IGD_disconnect();
    }
#endif
    if(wdaemon)
    {
        printf("Stopping Server... ");
        fflush(stdout);
        MHD_stop_daemon (wdaemon);
        printf("Done\n");
    }
#if (MHD_VERSION >= 0x00090301)
    if(user){free(user); user=NULL;}
    if(pass){free(pass); pass=NULL;}
#endif
#ifdef TH_MHDCAN_HTTPS
    if(G_key)   {free(G_key);   G_key=NULL;}
    if(G_cert)  {free(G_cert);  G_cert=NULL;}
#endif

#ifdef TH_FEATURE_TREE
    if(G_page){free(G_page); G_page=NULL;}
    if(G_date){free(G_date); G_date=NULL;}
#endif
    if(G_options){free(G_options); G_options=NULL;}
    if(pArr){free(pArr); pArr=NULL;}

    if (MODE == 6)
    {
        struct ltxt * cxt = txt;
        struct ltxt * old = NULL;
        while(cxt)
        {
            old = cxt;
            cxt = cxt->nxt;
            if(old->adir) free(old->adir);
            if(old != txt) free(old);
        }
        if(txt)
            free(txt);
    }
    printf("Exiting...\n");
    exit(0);
}
#ifdef USE_NORETURN
static noreturn void nulhandler()
#else
static void nulhandler()
#endif
{//Print configuration and basic usage when no arguments are given
    printf(PACKAGE_STRING);
    char support[FILENAME_MAX] = "\nConfiguration: ";
    strcat(support,"TH:[ ");
#ifndef TH_USE_FD_OPEN
    strcat(support,"F_OPEN ");
#endif
#ifdef TH_USE_FD_OPEN
    strcat(support,"OPEN ");
#endif
#ifdef TH_FEATURE_PIPE
    strcat(support,"PIPE ");
#endif
#ifdef TH_FEATURE_TREE
    strcat(support,"TREE ");
#endif
#ifdef TH_FEATURE_UPNP
    strcat(support,"UPNP ");
#endif
    strcat(support,"] ");
#if (MHD_VERSION >= 0x00094202)
    strcat(support,"MHD:[ ");
    if(MHD_is_feature_supported(MHD_FEATURE_MESSAGES)   == MHD_YES)
        strcat(support,"DEBUG ");
    if(MHD_is_feature_supported(MHD_FEATURE_IPv6)       == MHD_YES)
        strcat(support,"DUAL_STACK ");
    if(MHD_is_feature_supported(MHD_FEATURE_EPOLL)      == MHD_YES)
        strcat(support,"EPOLL ");
    if(MHD_is_feature_supported(MHD_FEATURE_SSL)        == MHD_YES)
        strcat(support,"HTTPS ");
    if(MHD_is_feature_supported(MHD_FEATURE_IPv6_ONLY)  == MHD_YES)
        strcat(support,"IPv6 ");
    if(MHD_is_feature_supported(MHD_FEATURE_LARGE_FILE) == MHD_YES)
        strcat(support,"LARGE_FILE ");
    if(MHD_is_feature_supported(MHD_FEATURE_POLL)       == MHD_YES)
        strcat(support,"POLL ");
    strcat(support,"] ");
#endif

    if(strlen(support)>16)
        printf("%s\n\n",support);

    printf("%s",TH_STR_CMD_HELP);
    exit(0);
}

static void verPrint(char * x)
{//Print certian command only when verbose flag is set
    if (verbose_flag)
        printf("%s",x);
    return;
}

#ifdef TH_FEATURE_INTBIND
static uint16_t randPort()
{ //Assign random port in private space (49152–65535)
    srand(time(NULL));
    uint16_t r = rand() % (65534 + 1 - 49152) + 49152;
    return r;
}
#endif

#ifndef _WIN32

    static void catcher (int sig) {}

    static void ignore_sigpipe ()
    {//Ignore SIGPIPE to prevent exits on connection interruption
        verPrint("SIGPIPE ERROR");

        struct sigaction oldsig;
        struct sigaction sig;

        sig.sa_handler = &catcher;
        sigemptyset (&sig.sa_mask);

#ifdef SA_INTERRUPT
            sig.sa_flags = SA_INTERRUPT;  // For SunOS
#else
            sig.sa_flags = SA_RESTART;
#endif

      if (0 != sigaction (SIGPIPE, &sig, &oldsig))
        printf ("Failed to install SIGPIPE handler!");
    }
#endif

#ifndef TH_FEATURE_INTBIND
static uint_least16_t getServerPort(struct MHD_Daemon * mhd)
{//Get servers port number after it has started
    const union MHD_DaemonInfo * f;
    f = MHD_get_daemon_info(mhd,MHD_DAEMON_INFO_LISTEN_FD);
    if (f != NULL)
    {
        MHD_socket sock = 0;
        memcpy(&sock,f,sizeof(MHD_socket));
        struct sockaddr_in sin;
        socklen_t len = sizeof(sin);
        if (getsockname(sock, (struct sockaddr *)&sin, &len) == -1)
            return 0;

        return ntohs(sin.sin_port);
    }
    return 0;
}
#endif

static void getLocalIP(char * protoId)
{//Get IP address of servers network adapter(s)

#ifndef _WIN32
    struct ifaddrs *ifap, *ifa;
    struct sockaddr_in *sa;
    char *addr;

    if (getifaddrs(&ifap) == -1)
    {
        printf("[ERR] Failed to detect network devices (%s)\n",
            strerror(errno));

        sigHandler(SIGINT);
    }
    for (ifa = ifap; ifa; ifa = ifa->ifa_next)
    {
        if (ifa->ifa_addr->sa_family==AF_INET)
        {
            sa = (struct sockaddr_in *) ifa->ifa_addr;
            addr = inet_ntoa(sa->sin_addr);
            printf("%s%s:%i (%s)\n",protoId,addr,PORT,ifa->ifa_name);
        }
    }
    freeifaddrs(ifap);
#else //Windows
    PIP_ADAPTER_INFO pAdapterInfo;
    PIP_ADAPTER_INFO pAdapter = NULL;
    int dwRetVal = 0;

    ULONG ulOutBufLen = sizeof (IP_ADAPTER_INFO);
    pAdapterInfo = (IP_ADAPTER_INFO *) MALLOC(sizeof (IP_ADAPTER_INFO));
    if(pAdapterInfo == NULL)
    {
        printf("Error fetching 'GetAdaptersinfo'\n");
    }
    if(GetAdaptersInfo(
        pAdapterInfo, &ulOutBufLen) == ERROR_BUFFER_OVERFLOW)
    {
        FREE(pAdapterInfo);
        pAdapterInfo = (IP_ADAPTER_INFO *) MALLOC(ulOutBufLen);
        if(pAdapterInfo == NULL)
        {
            printf("Error fetching 'GetAdaptersinfo'\n");
        }
    }
    if((dwRetVal =
        GetAdaptersInfo(pAdapterInfo, &ulOutBufLen)) == NO_ERROR)
    {
        pAdapter = pAdapterInfo;
        while (pAdapter)
        {
            if((strcmp
                (pAdapter->IpAddressList.IpAddress.String,"0.0.0.0")
                 ) != 0)

                printf("%s%s:%i (%s)\n",protoId,
                    pAdapter->IpAddressList.IpAddress.String,
                    PORT,pAdapter->Description);

            pAdapter = pAdapter->Next;
        }
    }
    else
        printf("GetAdaptersInfo failed with error: %d\n", dwRetVal);
    if (pAdapterInfo)
        FREE(pAdapterInfo);
#endif
}

static char * getClientIp(struct MHD_Connection *connection)
{ //Get remote connections IP address

    char * r;
    r = (char*) malloc(INET6_ADDRSTRLEN+1);

#ifdef _WIN32
    struct sockaddr *addr;
    addr = MHD_get_connection_info
        (connection,MHD_CONNECTION_INFO_CLIENT_ADDRESS)->client_addr;
    struct sockaddr_in *sin = (struct sockaddr_in *) addr;
    memcpy(r,inet_ntoa(sin->sin_addr),INET6_ADDRSTRLEN);
#elif (MHD_VERSION <= 0x00090600)
    struct sockaddr_in *addr;
    addr = MHD_get_connection_info
        (connection,MHD_CONNECTION_INFO_CLIENT_ADDRESS)->client_addr;
    memcpy(r,inet_ntoa(addr->sin_addr),INET6_ADDRSTRLEN);
#else
    struct sockaddr *addr;
    addr = MHD_get_connection_info
        (connection,MHD_CONNECTION_INFO_CLIENT_ADDRESS)->client_addr;
    inet_ntop(addr->sa_family, addr->sa_data + 2,
        r, INET6_ADDRSTRLEN);
#endif

    return r;
}

static unsigned char canRead(char * absdir)
{
#ifdef _WIN32 //TODO: Is there a Win32 equivilent?
    return 1;
#else
    struct stat st;
    if(!stat(absdir,&st))
    {
        if(st.st_mode & S_IRUSR)
            if(st.st_uid == getuid())
                return 1;
        if(st.st_mode & S_IRGRP)
            if(st.st_gid == getgid())
                return 2;
        if(st.st_mode & S_IROTH)
            return 3;
    }
    return 0;
#endif
}

static char * parseTime(time_t t)
{ //Get time_t and convert to RFC 1123 string
        char c[30];
        strftime(c, 30, "%a, %d %b %Y %H:%M:%S GMT",
            gmtime(&(t)));
        char *r = (char *) malloc(30);
        strncpy(r,c,30);
        return r;
}

static struct byteOffset parseOffset(const char * x
    ,unsigned long long max)
{ //Parse clients range request into a struct
    struct byteOffset r;
    r.ok = 1;

    char range[128];
    strncpy(range,x+6,128);

    char * me = strchr(range,',');
    if(me != NULL)
    {
        r.ok = 0;
    }

    char * e = strchr(range,'-');
    int i = -1;
    if(e != NULL)
    {
        i = (int)(e - range);
    }

    if(i != -1)
    {
        char * tmp;
        if(i == 0) // bytes=-100
        {
            r.offset_End = max -1;
            r.offset_Start = max - strtoul(range+1,&tmp,10);
        }
        else if (range[i+1] == '\0') // bytes=100-
        {
            r.offset_Start = strtoul(range,&tmp,10);
            r.offset_End = 0;
        }
        else // bytes=100-200
        {
            char * cStart = strrchr(x,'=');
            char * cEnd = strrchr(cStart,'-');

            r.offset_Start = strtoul(cStart+1,&tmp,10);
            r.offset_End = strtoul(cEnd+1,&tmp,10);

            cStart = NULL; cEnd = NULL;
        }
        tmp = NULL;
    }
    else
    {
        r.offset_Start = 0;
        r.offset_End = max;
        r.ok = 0;
    }

    if(r.offset_Start == 0
    || r.offset_Start > max-1
    || r.offset_End > max-1)
    {
        r.ok = 0;
    }

    if(r.offset_End <= 0 || r.offset_End >= max)
        r.offset_End = max-1;

    r.offset_Delta = r.offset_End - r.offset_Start + 1;
    if (r.ok == 1)
    snprintf
        (r.header,128,"bytes %llu-%llu/%llu",
            r.offset_Start,r.offset_End ,max);
    else
    snprintf
        (r.header,128,"*/%llu",max);
    return r;
}


static uint_fast8_t credOk(struct MHD_Connection *connection, char * ip)
{ //Check username and password are correct if applicable
    uint_fast8_t r = 0;
    if((FLAG & CRED) > 0)
    {
#if (MHD_VERSION >= 0x00090301) //If can password
    {
        char * u_user = NULL;
        char * u_pass = NULL;
        u_user = MHD_basic_auth_get_username_password
            (connection, &u_pass);

        if((u_user == NULL) || (u_pass == NULL))
        {
            r = 1;
        }
        else if((0 == strcmp (u_user, user))
             && (0 == strcmp (u_pass, pass))
            )
        {
            r = 0;
        }
        else r = 1;

        if(u_user) {free(u_user); u_user=NULL;}
        if(u_pass) {free(u_pass); u_pass=NULL;}
    }
#else
    {
        printf("[ERR] Password required but MHD dosen't support it\n");
        sigHandler(SIGINT);
    }
#endif
    }
    return r;
}

static int answer_mesg (void *cls, struct MHD_Connection *connection,
    const char *url, const char *method, const char *version,
    const char *upload_data, size_t *upload_data_size, void **con_cls)
{ //Respond to a request in Mesg/Link mode
    TH_GET_CLIENT_IP;

    struct MHD_Response *response;

    TH_REJECT_IF_CLIENT_NOT_COMPLIANT else
    {
        void * page = txt;
        TH_HTTP_CODE_PRNT(MHD_HTTP_OK);
        response = MHD_create_response_from_buffer
            (strlen (page), page, MHD_RESPMEM_PERSISTENT);

        MHD_add_response_header
                (response, MHD_HTTP_HEADER_CONNECTION, "close");

        r = MHD_queue_response (connection, MHD_HTTP_OK, response);
        MHD_destroy_response(response);
    }
    return r;
}

static char * getLastPath(char * path)
{
    char * ptr = strrchr( path, '/' );

#ifdef _WIN32 //Windows accepts \ as a directory in an address
    char * rptr = strrchr( path, '\\' );
    if (rptr > ptr)
    {
        ptr = rptr;
    }
#endif
    return ptr;
}

static char * getFileName(char * fileDir)
{ //Parse filename from a directory
    char * r = NULL;
    char fileName [FILENAME_MAX];
    char * ptr = getLastPath(fileDir);

    if (ptr)
    {
        snprintf(fileName,FILENAME_MAX,"%s\"%s\"",TH_CON_DIS,ptr + 1);
    }
    else
    {
        snprintf(fileName,FILENAME_MAX,"%s\"%s\"",TH_CON_DIS,fileDir);
    }

    char * tmp = (char *) malloc(strlen(fileName)+1);
    strncpy(tmp,fileName,strlen(fileName)+1);
    r = tmp;
    return r;
}

static int answer_file (void *cls, struct MHD_Connection *connection,
    const char *url, const char *method, const char *version,
    const char *upload_data, size_t *upload_data_size, void **con_cls)
{ //Respond to a request in File mode
    TH_GET_CLIENT_IP;

    struct MHD_Response *response = NULL;

    TH_REJECT_IF_CLIENT_NOT_COMPLIANT else
    {
        if(canRead(txt) == 0)
        {
            TH_HTTP_CODE_PRNT(MHD_HTTP_FORBIDDEN);
            TH_HTTP_CODE_POST(MHD_HTTP_FORBIDDEN);
            TH_HTTP_CODE_SEND(MHD_HTTP_FORBIDDEN);
            MHD_destroy_response(response);
            return MHD_YES;
        }
        struct stat fd_stat;
        if (stat(txt,&fd_stat) != 0)
        {
            TH_HTTP_CODE_PRNT(MHD_HTTP_NOT_FOUND);
            TH_HTTP_CODE_POST(MHD_HTTP_NOT_FOUND);
            TH_HTTP_CODE_SEND(MHD_HTTP_NOT_FOUND);
            MHD_destroy_response(response);
            return MHD_YES;
        }

        if(!S_ISREG(fd_stat.st_mode))
        {
            TH_HTTP_CODE_PRNT(MHD_HTTP_INTERNAL_SERVER_ERROR);
            TH_HTTP_CODE_POST(MHD_HTTP_INTERNAL_SERVER_ERROR);
            TH_HTTP_CODE_SEND(MHD_HTTP_INTERNAL_SERVER_ERROR);
            MHD_destroy_response(response);
            return MHD_YES;
        }

        TH_IF_FILE_OPEN(txt)
        {
            if (response)
            {
                TH_HTTP_CODE_PRNT(MHD_HTTP_NOT_FOUND);
                TH_HTTP_CODE_POST(MHD_HTTP_NOT_FOUND);
                TH_HTTP_CODE_SEND(MHD_HTTP_NOT_FOUND);
                MHD_destroy_response(response);
                return MHD_YES;
            }
            else
                return MHD_NO;
        }

        char * mdate = parseTime(fd_stat.st_mtime);

        const char * creqdate =
        MHD_lookup_connection_value
            (connection,MHD_HEADER_KIND,
            MHD_HTTP_HEADER_IF_MODIFIED_SINCE);

        TH_REJECT_304_IF_DATE_OK;

        const char * Range =
        MHD_lookup_connection_value
            (connection,MHD_HEADER_KIND,MHD_HTTP_HEADER_RANGE);

        if(Range != NULL)
        {
            struct byteOffset bytes = parseOffset(Range
                ,(unsigned long long)fsize);

            if(bytes.ok == 1)
            {
                fprintf(stdout,"[206] %s (%s)\n",ip,Range);
                TH_FILE_SERVE_OFFSET
                    (bytes.offset_Delta, fd, bytes.offset_Start);
                MHD_add_response_header
                    (response,MHD_HTTP_HEADER_CONNECTION,"close");
                r = MHD_queue_response
                    (connection,MHD_HTTP_PARTIAL_CONTENT,response);
            }
            else
            {
                response = MHD_create_response_from_buffer
                    (0,"",MHD_RESPMEM_PERSISTENT);
                TH_HTTP_CODE_PRNT
                    (MHD_HTTP_RANGE_NOT_SATISFIABLE);
                TH_HTTP_CODE_SEND
                    (MHD_HTTP_RANGE_NOT_SATISFIABLE);
            }
            MHD_add_response_header
                (response,MHD_HTTP_HEADER_CONTENT_RANGE,
                bytes.header);
        }
        else
        {
            TH_FILE_SERVE(fsize,fd);
        }

        MHD_add_response_header(response,
            MHD_HTTP_HEADER_CONTENT_DISPOSITION,TH_STATIC_FILENAME);

        MHD_add_response_header
            (response,MHD_HTTP_HEADER_ACCEPT_RANGES,"bytes");

        MHD_add_response_header(response,
            MHD_HTTP_HEADER_LAST_MODIFIED,mdate);

        TH_HTTP_HEADER_COMMON;

        free(mdate); mdate=NULL;

        if((FLAG & FFDL) > 0)
        {
            MHD_add_response_header
                (response,MHD_HTTP_HEADER_CONTENT_TYPE,MIMETYPE);
        }

        if (Range == NULL)
        {
            TH_HTTP_CODE_PRNT(MHD_HTTP_OK);
            MHD_add_response_header
                (response, MHD_HTTP_HEADER_CONNECTION, "close");
            r = MHD_queue_response
                (connection,MHD_HTTP_OK,response);
        }
        MHD_destroy_response (response);
    }
    return r;
}

static char * parseRemoveChar(char * msg, char * rem)
{//Remove all characters in rem from msg
    unsigned long mmax = strlen(msg);
    unsigned long rmax = strlen(rem);
    for(unsigned long x = 0; x < mmax;x++)
    {
        for(unsigned long y = 0; y < rmax;y++)
        {
            if(msg[x] == rem[y])
            {
                memmove(msg+x,msg+x+1,(strlen(msg)-x+1));
                mmax--;
                x--;
                break;
            }
        }
    }
    return msg;
}

static void parseHtmlText(char * Page)
{//Convert dangerious characters to their HTML entity equivilent
    unsigned long i;
    unsigned long max = strlen(Page);
    for(i = 0; i < max;i++)
    {
        char c = Page[i];
        switch(c)
        {
        case '&':
            memmove(Page+i+4,Page+i,(strlen(Page)-i+4));
            Page[i+1]='a';Page[i+2]='m';Page[i+3]='p';Page[i+4]=';';
            max = strlen(Page);
        break;
        case ' ':
            memmove(Page+i+5,Page+i,(strlen(Page)-i+5));
            Page[i]='&';Page[i+1]='n';Page[i+2]='b';
            Page[i+3]='s';Page[i+4]='p';Page[i+5]=';';
            max = strlen(Page);
        break;
        case '>':
            memmove(Page+i+3,Page+i,(strlen(Page)-i+3));
            Page[i]='&';Page[i+1]='g';Page[i+2]='t';Page[i+3]=';';
            max = strlen(Page);
        break;
        case '<':
            memmove(Page+i+3,Page+i,(strlen(Page)-i+3));
            Page[i]='&';Page[i+1]='l';Page[i+2]='t';Page[i+3]=';';
            max = strlen(Page);
        break;
        default:
        break;
        }
    }
    return;
}

static void parseHtmlLink(char * Page)
{//Convert dangerious characters to their URL encode equivilent
    unsigned long i;
    unsigned long max = strlen(Page);
    for(i = 0; i < max;i++)
    {
        char c = Page[i];
        switch(c)
        {
        case ' ': //Space
            memmove(Page+i+2,Page+i,(strlen(Page)-i+2));
            Page[i]='%';Page[i+1]='2';Page[i+2]='0';
            max = strlen(Page);
        break;
        case '!':
            memmove(Page+i+2,Page+i,(strlen(Page)-i+2));
            Page[i]='%';Page[i+1]='2';Page[i+2]='1';
            max = strlen(Page);
        break;
        case '"':
            memmove(Page+i+2,Page+i,(strlen(Page)-i+2));
            Page[i]='%';Page[i+1]='2';Page[i+2]='2';
            max = strlen(Page);
        break;
        case '#':
            memmove(Page+i+2,Page+i,(strlen(Page)-i+2));
            Page[i]='%';Page[i+1]='2';Page[i+2]='3';
            max = strlen(Page);
        break;
        case '$':
            memmove(Page+i+2,Page+i,(strlen(Page)-i+2));
            Page[i]='%';Page[i+1]='2';Page[i+2]='4';
            max = strlen(Page);
        break;
        case '%':
            memmove(Page+i+2,Page+i,(strlen(Page)-i+2));
            Page[i]='%';Page[i+1]='2';Page[i+2]='5';
            max = strlen(Page);
        break;
        case '&':
            memmove(Page+i+2,Page+i,(strlen(Page)-i+2));
            Page[i]='%';Page[i+1]='2';Page[i+2]='6';
            max = strlen(Page);
        break;
        case '\'':
            memmove(Page+i+2,Page+i,(strlen(Page)-i+2));
            Page[i]='%';Page[i+1]='2';Page[i+2]='7';
            max = strlen(Page);
        break;
        case '(':
            memmove(Page+i+2,Page+i,(strlen(Page)-i+2));
            Page[i]='%';Page[i+1]='2';Page[i+2]='8';
            max = strlen(Page);
        break;
        case ')':
            memmove(Page+i+2,Page+i,(strlen(Page)-i+2));
            Page[i]='%';Page[i+1]='2';Page[i+2]='9';
            max = strlen(Page);
        break;
        case '*':
            memmove(Page+i+2,Page+i,(strlen(Page)-i+2));
            Page[i]='%';Page[i+1]='2';Page[i+2]='A';
            max = strlen(Page);
        break;
        case '+':
            memmove(Page+i+2,Page+i,(strlen(Page)-i+2));
            Page[i]='%';Page[i+1]='2';Page[i+2]='B';
            max = strlen(Page);
        break;
        case ',':
            memmove(Page+i+2,Page+i,(strlen(Page)-i+2));
            Page[i]='%';Page[i+1]='2';Page[i+2]='C';
            max = strlen(Page);
        break;
        case '-':
            memmove(Page+i+2,Page+i,(strlen(Page)-i+2));
            Page[i]='%';Page[i+1]='2';Page[i+2]='D';
            max = strlen(Page);
        break;
        case '.':
            memmove(Page+i+2,Page+i,(strlen(Page)-i+2));
            Page[i]='%';Page[i+1]='2';Page[i+2]='E';
            max = strlen(Page);
        break;
        case '>':
            memmove(Page+i+2,Page+i,(strlen(Page)-i+2));
            Page[i]='%';Page[i+1]='3';Page[i+2]='E';
            max = strlen(Page);
        break;
        case '=':
            memmove(Page+i+2,Page+i,(strlen(Page)-i+2));
            Page[i]='%';Page[i+1]='3';Page[i+2]='D';
            max = strlen(Page);
        break;
        case '<':
            memmove(Page+i+2,Page+i,(strlen(Page)-i+2));
            Page[i]='%';Page[i+1]='3';Page[i+2]='C';
            max = strlen(Page);
        break;
        case '?':
            memmove(Page+i+2,Page+i,(strlen(Page)-i+2));
            Page[i]='%';Page[i+1]='3';Page[i+2]='F';
            max = strlen(Page);
        break;
        default:
        break;
        }
    }
    return;
}

static char * parseAbsoluteDirectory(char * wDir, char * get)
{//Get complete path of a file relative to the servers url path
    char * tmp = malloc(strlen(wDir)+strlen(get)+2);
    snprintf(tmp,strlen(wDir)+strlen(get)+2,
        "%s/%s",wDir,get);
    return tmp;
}

#ifdef TH_FEATURE_CRUMB
static char * build_crumb(const char * url)
{
    size_t s = strlen("<h3>\n<a href=\"/\">/</a>\n")+1;
    char * page = malloc(s);
    strncpy(page, "<h3>\n<a href=\"/\">/</a>\n", s);
    if(!strcmp(url,"/"))
    {
        s = strlen(page)+strlen("</h3>\n<hr>\n")+1;
        page = realloc(page,s);
        strncat(page, "</h3>\n<hr>\n", s);
        return page;
    }
    char * it = strchr(url,'/');

    while(it)
    {
        size_t s1, s2;
        char n[FILENAME_MAX] = "\0"; //File&nbsp;name
        char l[FILENAME_MAX] = "\0"; //File%2Dname
        it++;
        char * eit = strchr(it,'/');
        if(eit)
        {
            s1 = strlen(it) - strlen(eit);
            s2 = strlen(url) - strlen(eit);
        }
        else
        {
            s1 = strlen(it);
            s2 = strlen(url);
        }

        strncpy(n,it,s1+1);
        strncpy(l,url,s2);

        parseHtmlText(n);
        parseHtmlLink(l);

        s1 = strlen(n)+strlen(l)+strlen("<a href=\"/\">/</a>\n");
        char * tmp = malloc(s1+1);
        snprintf(tmp,s1,"<a href=\"%s\">%s</a>\n",l,n);
        page = realloc(page,strlen(page)+strlen(tmp)+1);
        strcat(page,tmp);
        free(tmp);
        it = eit;
    }
    char * tmp = malloc(strlen(page)+strlen("</h3>\n<hr>\n")+1);
    snprintf(tmp,strlen(page)+strlen("</h3>\n<hr>\n")+1
        ,"%s</h3>\n<hr>\n",page);
    free(page);
    return tmp;
}
#endif

static char * build_path(char * curdir, const char * url,
    const char * method, char * ip)
{
    size_t s = strlen(url)+strlen(TH_CSS_TABLE)
        +strlen(TH_STR_HTML_HEAD)+strlen(TH_STR_HTML_STYLE)
        +strlen(TH_STR_HEAD_FOOT)+strlen(TH_STR_BODY_HEAD)+1;
    char * page = malloc(s);

    snprintf(page,s
        ,TH_STR_HTML_HEAD TH_STR_HTML_STYLE TH_STR_HEAD_FOOT \
        TH_STR_BODY_HEAD, url, TH_CSS_TABLE);

    TH_HTTP_CODE_PRNT(MHD_HTTP_OK);
#ifdef TH_FEATURE_CRUMB
    if(FLAG & RECR || MODE == 6)
    {
        char * crumb = build_crumb(url);
        s = strlen(page)+strlen(crumb)+1;
        page = realloc(page, s);
        strncat(page, crumb, s);
        free(crumb);
    }
#endif

    page = realloc(page, strlen(page)+strlen(TH_STR_TABLE_HEAD)+1);
    strcat(page,TH_STR_TABLE_HEAD);

    uint it = 0;
    if(canRead(curdir) > 0)
    {
        TH_DIRENT_INIT(curdir)
        {
        #ifdef TH_FEATURE_SORT
            ep = sortep[dit];
        #endif
        #ifndef _DIRENT_HAVE_D_TYPE
            struct stat dtype;
            char c[FILENAME_MAX] = "\0";
            strncpy(c,curdir,FILENAME_MAX);
            strncat(c,"/",1);
            strncat(c,ep->d_name,FILENAME_MAX-strlen(c));
            stat(c, &dtype);
            if((ep->d_name[0] != '.') && ((((FLAG & RECR) > 0) ||
                S_ISREG(dtype.st_mode)))
            )
        #else
            if((ep->d_name[0] != '.') && ((((FLAG & RECR) > 0))
            || (ep->d_type == DT_REG)))
        #endif // _DIRENT_HAVE_D_TYPE
            {
                char n[FILENAME_MAX] = "\0"; //File&nbsp;name
                char l[FILENAME_MAX] = "\0"; //File%2Dname
                char u[FILENAME_MAX] = "\0"; //URL
                unsigned long long s    = 0; //Sizeof tmp array
                unsigned long long fs   = 0; //Literal byte size
                unsigned long long fsd  = 1; //strlen of uint fs
                char * tmp = NULL;

                strcpy(n,ep->d_name);
                char * aDir = parseAbsoluteDirectory(curdir,n);
                struct stat fd_stat;

                if (stat(aDir,&fd_stat) != 0)
                {
                    free(aDir);aDir=NULL;
    #ifdef TH_FEATURE_SORT
                    dit++;
    #endif
                    continue;
                }

                strcpy(l,n);

                if(S_ISDIR(fd_stat.st_mode))
                {
                    strcat(n,"/");
                }
                else if(S_ISREG(fd_stat.st_mode))
                {
                fs = (unsigned long long)fd_stat.st_size;
                    if (fs > 0)
                        fsd = (unsigned long long)
                            (floor(log10(fs)) + 1);
                }
                else
                {
                    free(aDir);aDir=NULL;
    #ifdef TH_FEATURE_SORT
                    dit++;
    #endif
                    continue;
                }

                strcpy(u,url);
                parseHtmlText(n);
                parseHtmlLink(l);
                char * d = parseTime(fd_stat.st_mtime);

                if(strcmp(url,"/") == 0)
                {
                    s = strlen(n)+strlen(l)+strlen(d)+(fsd)+
                    strlen("<tr><td><a href=\"\">%s</a>"
                        "</td><td>%s</td><td></td></tr>\n");

                    tmp = malloc(s+1);

                    snprintf(tmp,s,
                    "<tr><td><a href=\"%s\">%s</a>"
                    "</td><td>%s</td><td>%llu</td></tr>\n"
                    ,l,n,d,fs);
                }
                else
                {
                    parseHtmlLink(u);
                    s = strlen(n)+strlen(l)+strlen(u)
                        +strlen(d)+(fsd)+
                        strlen("<tr><td><a href=\"/\"></a></td>"
                            "<td>%s</td><td></td></tr>\n");

                    tmp = (char*) malloc(s+1);

                    snprintf(tmp,s,
                    "<tr><td><a href=\"%s/%s\">%s</a></td>"
                    "<td>%s</td><td>%llu</td></tr>\n"
                        ,u,l,n,d,fs);
                }
                page = (char*)
                realloc(page,
                    ((strlen(page)
                    +strlen(tmp)+1)));
                strcat(page,tmp);
                free(tmp);  tmp=NULL;
                free(aDir); aDir=NULL;
                free(d);    d=NULL;
                it++;
            }
            TH_DIRENT_CLEANUP_ITERATION();
        } //TH_DIRENT_INIT()
        TH_DIRENT_CLEANUP();
    }
    if(it == 0)
    {
        char tmp[] = "<tr><td>Empty Directory</td></tr>\n";
        page = (char*) realloc
            (page,(strlen(page)+strlen(tmp))+1);

        strcat(page,tmp);
    }
    s = strlen(page)+strlen(TH_STR_TABLE_FOOT)+strlen(TH_STR_HTML_FOOT)
        +1;

    page = realloc(page,s);
    strncat(page,TH_STR_TABLE_FOOT TH_STR_HTML_FOOT,s);
    return page;
}

static int answer_path (void *cls, struct MHD_Connection *connection,
    const char *url, const char *method, const char *version,
    const char *upload_data, size_t *upload_data_size, void **con_cls)
{ //Respond to a request with a html list of a directory(s)
    TH_GET_CLIENT_IP;

    struct MHD_Response *response = NULL;
    char * page = NULL;

    TH_REJECT_IF_CLIENT_NOT_COMPLIANT else
    {
        char * curdir = malloc((strlen(txt)+strlen(url))+1);
        strcpy(curdir,txt);
        if(strlen(url) > 1)
            strcat(curdir,url);
        struct stat curStat;
        if (stat(curdir,&curStat) != 0)
        {
            TH_HTTP_CODE_PRNT(MHD_HTTP_NOT_FOUND);
            TH_HTTP_CODE_POST(MHD_HTTP_NOT_FOUND);
            TH_HTTP_CODE_SEND(MHD_HTTP_NOT_FOUND);
            MHD_destroy_response(response);
            free(curdir);curdir = NULL;
            return MHD_YES;
        }

        if(S_ISREG(curStat.st_mode))
        {
            TH_DYNAMIC_FILE(curdir,curStat);
        }
        else if(S_ISDIR(curStat.st_mode))
        {
            if(((FLAG & RECR) <= 0) && ((strlen(url) > 1)))
            {
                TH_HTTP_CODE_PRNT(MHD_HTTP_FORBIDDEN);
                //Intentionally lie to the remote, there is no file here
                TH_HTTP_CODE_POST(MHD_HTTP_NOT_FOUND);
                TH_HTTP_CODE_SEND(MHD_HTTP_NOT_FOUND);
                MHD_destroy_response(response);
                free(curdir); curdir=NULL;
                return r;
            }
			if(method[0] == 'H') //strcmp(method,"HEAD") but faster
			{
				TH_HTTP_CODE_PRNT(MHD_HTTP_OK);
				TH_HTTP_CODE_POST(MHD_HTTP_OK);
				TH_HTTP_CODE_SEND(MHD_HTTP_OK);
				MHD_destroy_response(response);
				free(curdir);curdir = NULL;
				return MHD_YES;
			}
            page = build_path(curdir,url,method,ip);
        }
        //End directory stuff
        if(page)
        {
        response = MHD_create_response_from_buffer
            (strlen (page),(void*) page,MHD_RESPMEM_MUST_FREE);
        MHD_add_response_header
            (response,MHD_HTTP_HEADER_CONTENT_TYPE,
            "text/html; charset=utf-8");
        }
        free(curdir);curdir = NULL;
        r = MHD_queue_response (connection, MHD_HTTP_OK, response);
        MHD_destroy_response(response);
    }
    return r;
}

static int answer_list(void *cls, struct MHD_Connection *connection,
    const char *url, const char *method, const char *version,
    const char *upload_data, size_t *upload_data_size, void **con_cls)
{ //Respond to a request by looking up url and refering it to handler
    TH_GET_CLIENT_IP;

    struct MHD_Response * response = NULL;
    char * page = NULL;

    TH_REJECT_IF_CLIENT_NOT_COMPLIANT else
    {
        struct ltxt * cxt = txt;
        if(strcmp(url,"/") == 0)
        {
            char * page = G_page;
            const char * creqdate
                = MHD_lookup_connection_value(connection
                ,MHD_HEADER_KIND,MHD_HTTP_HEADER_IF_MODIFIED_SINCE);

            TH_REJECT_304_IF_GDATE_OK;

            response = MHD_create_response_from_buffer
                (strlen (page),(void*) page,MHD_RESPMEM_PERSISTENT);

            TH_HTTP_CODE_PRNT(MHD_HTTP_OK);
            MHD_add_response_header
                (response,MHD_HTTP_HEADER_CONTENT_TYPE,
                "text/html; charset=utf-8");
            MHD_add_response_header(response,
                MHD_HTTP_HEADER_LAST_MODIFIED,G_date);
            r = MHD_queue_response (connection, MHD_HTTP_OK, response);
            MHD_destroy_response(response);
            return r;
        }

        char * dir = NULL;
        while(cxt)
        {
            char * t = malloc(strlen(cxt->path)+1);
            memcpy(t,cxt->path,strlen(cxt->path)+1);
            if(t[strlen(t)-1] == '/')
                t[strlen(t)-1] = '\0';

            if(strstr(url+1,t) == url+1)
            {
                size_t u = strlen(t)+1;
                size_t s = strlen(cxt->adir)+strlen(url)-u+1;

                dir = malloc(s);
                strncpy(dir,cxt->adir,s);
                strncat(dir,url+u,s);
                free(t);
                break;
            }
            free(t);
            cxt = cxt->nxt;
        }

        if(dir)
        {
            struct stat st;
            if(stat(dir, &st) == 0)
            {
                char * curdir = dir;

                if(S_ISREG(st.st_mode))
                {
                    TH_DYNAMIC_FILE(curdir,st);
                }
                else if(S_ISDIR(st.st_mode))
                {
                    page = build_path(curdir,url,method,ip);
                    if(page)
                    {
                        response = MHD_create_response_from_buffer
                            (strlen (page),(void*) page
                            ,MHD_RESPMEM_MUST_FREE);
                        MHD_add_response_header
                            (response,MHD_HTTP_HEADER_CONTENT_TYPE,
                            "text/html; charset=utf-8");
                    }
                    free(curdir);curdir = NULL;
                    r = MHD_queue_response (connection,
                        MHD_HTTP_OK, response);
                    MHD_destroy_response(response);
                    return r;
                }
                else
                {
                    free(curdir);curdir = NULL;
                    TH_HTTP_CODE_PRNT(MHD_HTTP_NOT_FOUND);
                    TH_HTTP_CODE_POST(MHD_HTTP_NOT_FOUND);
                    TH_HTTP_CODE_SEND(MHD_HTTP_NOT_FOUND);
                    MHD_destroy_response(response);
                    return MHD_YES;
                }
            }
            else
                free(dir);
        }
        else
        {
            TH_HTTP_CODE_PRNT(MHD_HTTP_NOT_FOUND);
            TH_HTTP_CODE_POST(MHD_HTTP_NOT_FOUND);
            TH_HTTP_CODE_SEND(MHD_HTTP_NOT_FOUND);
            MHD_destroy_response(response);
            return MHD_YES;
        }
    }
    return MHD_NO;
}

#ifdef TH_FEATURE_TREE
static char * folderScan(char * dir, char * baseurl)
{//Scan a folder for files and convert to html listing (for Tree)
    //Directory stuff
    char * page = NULL;
    uint it = 0;

    TH_DIRENT_INIT(dir)
    {

#ifdef TH_FEATURE_SORT
        ep = sortep[dit];
#endif
        size_t s = 0;
        char * tmp = NULL;

        if(ep->d_name[0] != '.')
        {
#ifndef _DIRENT_HAVE_D_TYPE
            struct stat dtype;
            char dircat[FILENAME_MAX];
            strcpy(dircat,dir);
            strcat(dircat,"/");
            strcat(dircat,ep->d_name);

            if(stat(dircat, &dtype) != 0)
            {
                printf("Failed\n");
                sigHandler(SIGINT);
            }
            if(S_ISDIR(dtype.st_mode))
#else
            if(ep->d_type == DT_DIR)
#endif // _DIRENT_HAVE_D_TYPE
            {
                char a[FILENAME_MAX];
                char b[FILENAME_MAX];
                strcpy(a,dir);
                strcat(a,"/");
                strcat(a,ep->d_name);

                if(strcmp(baseurl,"\0") == 0)
                    snprintf(b,FILENAME_MAX,"%s/%s/",
                        baseurl,ep->d_name);
                else
                    snprintf(b,FILENAME_MAX,"%s%s/",
                        baseurl,ep->d_name);

                tmp = folderScan(a,b);
                if (tmp != NULL)
                {
                    s = strlen(tmp)+1;

                    if(page == NULL)
                    {
                        page = malloc(s+1);
                        strncpy(page,tmp,s);
                    }
                    else
                    {
                        s+=strlen(page)+1;
                        page = (char*) realloc(page,s+1);
                        strncat(page,tmp,s+1);
                    }
                    free(tmp);tmp=NULL;
                }
            }
            else
            {
                char n[FILENAME_MAX] = "\0";
                char l[FILENAME_MAX] = "\0";

                strcpy(n,baseurl);
                strcat(n,ep->d_name);
                strcpy(l,n);

                parseHtmlText(n);
                parseHtmlLink(l);

                s = strlen(n)+strlen(l)+
                    strlen("<a href=\"/\">/</a><br />\n");

                tmp = (char*) malloc(s+1);
                    snprintf(tmp,s,"<a href=\"%s\">"
                        "%s</a><br />\n",l,n);

                if(page == NULL)
                {
                    page = malloc(s+1);
                    strncpy(page,tmp,s);
                }
                else
                {
                    s+=strlen(page)+1;
                    page = realloc(page,s+1);
                    strncat(page,tmp,s);
                }
                free(tmp);tmp=NULL;
            }
        it++;
        }
        TH_DIRENT_CLEANUP_ITERATION();
    } //TH_DIRENT_INIT

    TH_DIRENT_CLEANUP();
    return page;
}
#endif

static char * build_list()
{
    struct ltxt * cxt = txt;
	size_t s = strlen("/")+strlen(TH_STR_HTML_HEAD)
		+strlen(TH_STR_HTML_STYLE)+strlen(TH_STR_HEAD_FOOT)
		+strlen(TH_STR_BODY_HEAD)+strlen(TH_CSS_GLOBAL)+1;
    char * page = malloc(s);

    snprintf(page,s
        ,TH_STR_HTML_HEAD TH_STR_HTML_STYLE \
        TH_STR_HEAD_FOOT TH_STR_BODY_HEAD \
        ,"/", TH_CSS_GLOBAL);

    char * tmp = build_crumb("/");
    if(tmp)
	{
		size_t s = strlen(page)+strlen(tmp)+1;
		page = realloc(page,s);
		strncat(page,tmp,s);
		free(tmp);
	}
    while (cxt)
    {
        tmp = NULL;
        char n[FILENAME_MAX] = "\0";
        char l[FILENAME_MAX] = "\0";
        unsigned long s = 0;

        strcpy(n,cxt->path);
        strcpy(l,n);
        if(l[strlen(l)-1] == '/')
            l[strlen(l)-1] = '\0';

        parseHtmlText(n);
        parseHtmlLink(l);

        s = strlen(n)+strlen(l)+
            strlen("<a href=\"/\">/</a><br />\n");
        tmp = malloc(s+1);
        snprintf(tmp,s,"<a href=\"%s\">"
            "%s</a><br />\n",l,n);
        s+=strlen(page)+1;
        page = realloc(page,s+1);
        strncat(page,tmp,s+1);
        free(tmp);
        cxt = cxt->nxt;
    }

    page = realloc(page,
        strlen(page)+strlen(TH_STR_HTML_FOOT)+1);
    strncat(page,TH_STR_HTML_FOOT,strlen(page)
        +strlen(TH_STR_HTML_FOOT)+1);
    return page;
}

static void build_list_struct(char * str)
{
    struct stat st;
    if(stat(str, &st))
        return;

    char * s = getLastPath(str);
    if(!s || strlen(str) < 2)
        s = str;
    else if(s == str+strlen(str)-1)
    {
        char * t = malloc(strlen(str)+1);
        memcpy(t,str,strlen(str)+1);
        t[strlen(str)-1] = '\0';
        size_t i = strlen(t)-strlen(getLastPath(t));
        s = str+i+1;
        free(t);
    }
    else
        s++;

    if(strcmp(s,".") == 0)
        s = "!w";

    if(strcmp(s,"/") == 0)
        s = "!r";

    if(S_ISREG(st.st_mode) || S_ISDIR(st.st_mode))
    {
        struct ltxt * cxt = txt;
        if(!cxt)
        {
            txt = (struct ltxt *) malloc(sizeof(struct ltxt));
            cxt = txt;
            cxt->nxt = NULL;
            cxt->path = s;
            cxt->adir = realpath(str,NULL);
            return;
        }

        while (cxt->nxt)
            cxt = cxt->nxt;

        cxt->nxt = malloc(sizeof(struct ltxt));
        cxt->nxt->nxt = NULL;
        cxt->nxt->adir = NULL;
        cxt->nxt->path = s;
        cxt->nxt->adir = realpath(str,NULL);
    }
}

#ifdef TH_FEATURE_TREE
static char * build_tree ()
{ //Respond to a request with a html list of a directory(s)
    printf("Building Tree... ");
    fflush(stdout);
    char * page = NULL;
    char * tmp = "\0";
    tmp = folderScan(txt,"/");
    if(tmp == NULL)
    {
        printf("Failed\n");
        sigHandler(SIGINT);
        return NULL;
    }
    size_t len = 0;
    len = strlen(TH_STR_HTML_HEAD)+strlen(tmp)
        +strlen(TH_STR_HTML_FOOT)+1;
    page = malloc(len);
    snprintf(page,len,TH_STR_HTML_HEAD"%s%s","/",
        tmp,TH_STR_HTML_FOOT);
    printf("Done\n");
    free(tmp);tmp=NULL;
    return page;
}

static int answer_tree (void *cls, struct MHD_Connection *connection,
    const char *url, const char *method, const char *version,
    const char *upload_data, size_t *upload_data_size, void **con_cls)
{ //Respond to a request with a html list of directory(s)
    TH_GET_CLIENT_IP;

    struct MHD_Response *response = NULL;

    TH_REJECT_IF_CLIENT_NOT_COMPLIANT else
    {
        char * curdir = malloc((strlen(txt)+strlen(url))+1);
        strcpy(curdir,txt);
        if(strlen(url) > 1)
            strcat(curdir,url);
        struct stat curStat;
        if (stat(curdir,&curStat) != 0)
            {
                TH_HTTP_CODE_PRNT(MHD_HTTP_NOT_FOUND);
                TH_HTTP_CODE_POST(MHD_HTTP_NOT_FOUND);
                TH_HTTP_CODE_SEND(MHD_HTTP_NOT_FOUND);
                MHD_destroy_response(response);
                free(curdir);curdir = NULL;
                return MHD_YES;
            }

        if(S_ISREG(curStat.st_mode))
        {
            TH_DYNAMIC_FILE(curdir,curStat);
        }
        else if(S_ISDIR(curStat.st_mode))
        {
            const char * creqdate
                = MHD_lookup_connection_value(connection
                ,MHD_HEADER_KIND,MHD_HTTP_HEADER_IF_MODIFIED_SINCE);

            TH_DYNAMIC_REJECT_304_IF_GDATE_OK;

            TH_HTTP_CODE_PRNT(MHD_HTTP_OK);
            response = MHD_create_response_from_buffer
                (strlen (G_page),(void*) G_page,MHD_RESPMEM_PERSISTENT);

            MHD_add_response_header(response,
                MHD_HTTP_HEADER_LAST_MODIFIED,G_date);

            r = MHD_queue_response (connection, MHD_HTTP_OK, response);
            MHD_destroy_response(response);
        }
        free(curdir);curdir=NULL;
    }
return r;
}
#endif

static void messageMode()
{//Print a Message: or Link: to stdout
    if(MODE==0)
        printf("Message: ");
    else if (MODE==1)
        printf("Link: ");

    printf("%s\n",(char *) txt);
}

static void autoMode()
{//Determine what mode to start the server in depending on arguments
    struct stat txt_stat;
    if((stat(txt, &txt_stat) == 0)
    && ((S_ISREG(txt_stat.st_mode))
    || (S_ISDIR(txt_stat.st_mode))))
    {
        if(S_ISREG(txt_stat.st_mode) == 1)
        {
            MODE = 2; //File mode
        }
        else if (S_ISDIR(txt_stat.st_mode) == 1)
        {
            MODE = 3; //Path mode
        }
    }
    else
    {
        if(strstr(txt,"://") != NULL)
        {
            MODE = 1; //Link Mode
        }
        else
        {
            MODE = 0; //Message mode
        }
    }
}

static void setMode()
{//Configure mode parameters or call autoMode() to determine it
    struct stat testStat;
    switch (MODE)
    {
    case 0:
        messageMode();
    break;

    case 1:
        messageMode();
        char * ttxt = malloc((strlen(txt)*2) + 16);
        sprintf(ttxt,"<a href='%s'>%s</a>",(char *) txt,(char *) txt);
        strcpy(txt,ttxt);
        free(ttxt);
        ttxt = NULL;
    break;

    case 2:
        TH_MODE_PRINT(File);
        char * t = getFileName(txt);
        strcpy(TH_STATIC_FILENAME,t);
        free(t); t=NULL;
        if(stat(txt,&testStat) != 0)
        {
            printf("Note: Cannot access this file currently, (%s)\n",
                strerror(errno));
        }
        else if(!S_ISREG(testStat.st_mode))
        {
            printf("Note: %s exists but isn't a file\n",(char *) txt);
        }
    break;

    case 3:
        TH_MODE_PRINT(Path);
        /*TH_SESSION_HEADER = TH_STR_HTML_HEAD \
            "<style>" TH_CSS_TABLE "</style>\n" TH_STR_HEAD_FOOT;
        TH_SESSION_FOOTER = TH_STR_TABLE_FOOT TH_STR_HTML_FOOT;*/
        if(stat(txt,&testStat) != 0)
        {
            printf
                ("Note: Cannot access this directory currently, (%s)\n",
                strerror(errno));
        }
        else if(!S_ISDIR(testStat.st_mode))
        {
            printf("Note: %s exists but isn't a directory",
                (char *) txt);

            if(S_ISREG(testStat.st_mode))
                printf(". Since it's a file, "
                    "it will still be accessable from server root");
            printf("\n");
        }
        break;
#ifdef TH_FEATURE_TREE
    case 4:
        TH_MODE_PRINT(Tree);
        G_page = build_tree();
        G_date = parseTime(time(NULL));
        /*TH_SESSION_HEADER = TH_STR_HTML_HEAD;
        TH_SESSION_FOOTER = TH_STR_HTML_FOOT;*/
        break;
#endif
#ifdef TH_FEATURE_PIPE
    case 5:
        TH_MODE_PRINT(Pipe);
        break;
#endif
    case 6:
        printf("List: \n");
        G_date = parseTime(time(NULL));
        G_page = build_list();
        break;
    default:
        autoMode(); //MODE will be set to a valid option
        setMode();  //Recursion will have valid MODE value
    }
}

#if (MHD_VERSION >= 0x00093904)
static void onHttpNotify(void *cls, struct MHD_Connection *connection,
        void **con_cls, enum MHD_ConnectionNotificationCode code)
{//Print to stdout that the client has completed/stopped their transfer
    TH_GET_CLIENT_IP;
    char * x = *con_cls;
    if(x && x[0] == 'G')
    {
        char * p = strstr(x,":/");
        if(p)
            fprintf(stdout,"[%c000] %s%s\n",x[0],ip,p+1);
        else
            fprintf(stdout,"[%c000] %s\n",x[0],ip);
    }
    if(x){free(x); x=NULL;}
}
#endif

static void startServer()
{//Setup configuration and start the server

    //Setup MHD features...
#if (MHD_VERSION >= 0x00094202)
    if(MHD_is_feature_supported(MHD_FEATURE_POLL) == MHD_YES)
        wMode |= MHD_USE_POLL; //Use Polling when available
#endif

    //Setup MHD options
    uint optit = 0;
    G_options = calloc(1,sizeof(struct MHD_OptionItem));
    if(G_options == NULL)
        sigHandler(SIGINT);
    //Don't allow port to be occupied by another process
#ifdef  MHD_OPTION_LISTENING_ADDRESS_REUSE
    G_options[optit].option = MHD_OPTION_LISTENING_ADDRESS_REUSE;
    G_options[optit].value  = 0;
    G_options[optit].ptr_value = NULL;
    optit++;
#endif
#if (MHD_VERSION >= 0x00093904)
    G_options = realloc(G_options,sizeof
        (struct MHD_OptionItem)*(optit+1));
    G_options[optit].option = MHD_OPTION_NOTIFY_COMPLETED;
    G_options[optit].value  = (intptr_t) &onHttpNotify;
    G_options[optit].ptr_value = NULL;
    optit++;
#endif
#ifdef TH_MHDCAN_HTTPS
    if(((FLAG & HSSL) > 0) && (G_key != NULL) && (G_cert != NULL))
    {
    wMode |= MHD_USE_SSL;

    G_options = realloc(G_options,sizeof
        (struct MHD_OptionItem)*(optit+1));

    G_options[optit].option = MHD_OPTION_HTTPS_MEM_KEY;
    G_options[optit].value  = (intptr_t) NULL;
    G_options[optit].ptr_value = G_key;
    optit++;

    G_options = realloc(G_options,sizeof
        (struct MHD_OptionItem)*(optit+1));
    G_options[optit].option = MHD_OPTION_HTTPS_MEM_CERT;
    G_options[optit].value  = (intptr_t) NULL;
    G_options[optit].ptr_value = G_cert;
    optit++;
    }
#endif
    G_options = realloc(G_options,sizeof
        (struct MHD_OptionItem)*(optit+1));

    G_options[optit].option = MHD_OPTION_END;
    G_options[optit].value = 0;
    G_options[optit].ptr_value = NULL;

#ifndef NDEBUG
    wMode |= MHD_USE_DEBUG;
#endif
    switch (MODE)
    {
    case 0:
    case 1:
#ifdef TH_FEATURE_PIPE
    case 5:
#endif
        wdaemon = MHD_start_daemon
        (wMode,PORT,NULL,NULL,&answer_mesg,
            NULL,MHD_OPTION_ARRAY,G_options,MHD_OPTION_END);
        break;
    case 2:
        wdaemon = MHD_start_daemon
        (wMode,PORT,NULL,NULL,&answer_file,
            NULL,MHD_OPTION_ARRAY,G_options,MHD_OPTION_END);
        break;
    case 3:
        wdaemon = MHD_start_daemon
        (wMode,PORT,NULL,NULL,&answer_path,
            NULL,MHD_OPTION_ARRAY,G_options,MHD_OPTION_END);
    break;
#ifdef TH_FEATURE_TREE
    case 4:
        wdaemon = MHD_start_daemon
        (wMode,PORT,NULL,NULL,&answer_tree,
            NULL,MHD_OPTION_ARRAY,G_options,MHD_OPTION_END);
    break;
#endif
    case 6:
        wdaemon = MHD_start_daemon
        (wMode,PORT,NULL,NULL,&answer_list,
            NULL,MHD_OPTION_ARRAY,G_options,MHD_OPTION_END);
    break;
    default: wdaemon = NULL;
        break;
    }
    if(G_options){free(G_options); G_options=NULL;}
}

#ifdef TH_MHDCAN_HTTPS
static char * loadText(const char *filename,uint size)
{//Load the entirity of a file into a char*
    FILE *fp;
    char *buffer;

    if (size == 0)
        return NULL;

    fp = fopen (filename, "rb");
    if (! fp)
        return NULL;

    buffer = malloc (size + 1);
    if (! buffer)
    {
        fclose (fp);
        return NULL;
    }
    buffer[size] = '\0';

    if (size != fread (buffer, 1, size, fp))
    {
        free (buffer);
        buffer = NULL;
    }
    fclose (fp);
    return buffer;
}

static char * getUsrConfigFile(char * usrHomeDir,char * value)
{//Load text file into a char, must free manually
    char * r = NULL;
    struct stat tmp_stat;
    //Try absolute/relative to work directory for file
    if (stat(value,&tmp_stat) == 0
    && S_ISREG(tmp_stat.st_mode))
    {
        r = loadText(value,(unsigned int)tmp_stat.st_size);
        return r;
    }
    //Try relative to settings directory for file
    char * tmp_dir = NULL;  char * pch = NULL;
    pch = strrchr(usrHomeDir,'/');
    if(pch)
    {
        unsigned long tmp_strSize = (strlen(usrHomeDir)-strlen(pch))+1;
        char * dir = malloc(tmp_strSize);
        strncpy(dir,usrHomeDir,tmp_strSize);
        dir[tmp_strSize-1] = '\0';
        tmp_strSize += strlen(value)+2;
        tmp_dir = malloc(tmp_strSize);
        snprintf(tmp_dir,tmp_strSize,"%s/%s",dir,value);
        free(dir);
    }
    if (tmp_dir
    && stat(tmp_dir,&tmp_stat) == 0
    && S_ISREG(tmp_stat.st_mode))
    {
        r = loadText(tmp_dir,(unsigned int) tmp_stat.st_size);
    }
    else
    {
        r = NULL;
    }
    if(tmp_dir){free(tmp_dir); tmp_dir=NULL;}
    return r;
}
#endif

static void new_port()
{
    if(pArr == NULL)
    {
        pArr = calloc(1, sizeof(unsigned short));
    }
    else
    {
        unsigned short * p =
            realloc(pArr,sizeof(unsigned short)*(pit+1));
        if(p)
        {
            pArr = p;
        }
        else
            printf("Out of memory!\n");
    }
    pit++;
}

static void parsePorts(char * p)
{
    char * str = malloc(strlen(p)+1);
    strncpy(str,p,strlen(p)+1);
    str = parseRemoveChar(str,"\n\t");
    char * commaret = NULL;
    char * commatok = NULL;
    commatok = strtok_r(str,",",&commaret);
    while(commatok != NULL)
    {
        char * dashtok = NULL;
        if((dashtok = strstr(commatok,"-")) != NULL)
        {
            char num1[6] = "";
            char num2[6] = "";

            strncpy(num1,commatok,strlen(commatok)-strlen(dashtok));
            strncpy(num2,dashtok+1,strlen(dashtok)-1);

            unsigned short n1 = (unsigned short)atoi(num1);
            unsigned short n2 = (unsigned short)atoi(num2);

            if(n2 > n1)
            {
                for(unsigned short i = n1; i <= n2; i++)
                {
                    new_port();
                    pArr[pit-1] = i;
                }
            }
            else
            {
                for(unsigned short i = n1; i >= n2; i--)
                {
                    new_port();
                    pArr[pit-1] = i;
                }
            }
        }
        else
        {
            char num[6] = "";
            strncpy(num,commatok,strlen(commatok));
            unsigned short t = (unsigned short) atoi(num);
            if(t > 0)
            {
                new_port();
                pArr[pit-1] = t;
            }
        }
        commatok = strtok_r(NULL,",",&commaret);
    }
    if(str){free(str);str=NULL;}
}

static void getUsrConfig()
{//Load configuration from user/system settings file
    char usrHomeDir[FILENAME_MAX] = "\0";
    char * envp = NULL;
    struct stat s_stat;
    envp = getenv(TH_STR_ENVDIR);
    if (envp != NULL)
    {
        strncpy(usrHomeDir,envp,strlen(envp));
#ifdef _WIN32
        strcat(usrHomeDir,"/th/th.conf");
#else
        strcat(usrHomeDir,"/.config/th/th.conf");
#endif
    }
    if ((strcmp(usrHomeDir,"") == 0) || (stat(usrHomeDir,&s_stat) != 0))
    {
#ifdef _WIN32
                char tmp[FILENAME_MAX];
                if (GetModuleFileName(NULL,tmp,FILENAME_MAX-1) < 1)
                        return;
                for (int i = strlen(tmp); tmp[i] != '\\'; i--)
                        {tmp[i] = '\0';}
                strncat(tmp,"th.conf",FILENAME_MAX-1);
                strncpy(usrHomeDir,tmp,FILENAME_MAX-1);
#else
        strcpy(usrHomeDir,"/etc/th/th.conf");
#endif
        if (stat(usrHomeDir,&s_stat) != 0)
        {
            return;
        }
    }
    FILE * usrConfig = fopen(usrHomeDir,"r");
    char line[128];
    if(usrConfig != NULL)
    {
        char * option = NULL;
        char * value = NULL;

        while (fgets(line, sizeof(line), usrConfig))
        {
            unsigned long i = 0;
            for (; i < strlen(line); i++)
            {
                if(line[i] == '#')
                {
                    line[i]= '\0';
                    break;
                }
            }
            if(strlen(line) == 0)
                continue;

            option  = strtok(line,"=");
            value   = strtok(NULL,"=");

            if(option && value)
            {
                parseRemoveChar(value,"\r\n");
                if((PORT == 0) && (pArr == NULL)
                && (strstr(option,"port") != NULL))
                {
                    if((strstr(value,"-") == NULL)
                    && (strstr(value,",") == NULL))
                    {
                        int tmp_port;
                        TH_CHECK_PORT_VALID(value,tmp_port,PORT);
                    }
                    else
                        parsePorts(value);
                }
#ifndef TH_USE_FD_OPEN
                if(strstr(option,"buffer") != NULL)
                {
                    TH_FILE_BUFFER = (unsigned int) strtol
                        (value, NULL, 10);
                    if(TH_FILE_BUFFER < 1)
                    {
                        printf("[ERR] Invalid buffer size\n");
                        sigHandler(SIGINT);
                    }
                }
#endif
#if (MHD_VERSION >= 0x00090301)
                if((!user) && (strstr(option,"username") != NULL))
                {
                    if(value[strlen(value)-1] == '\n')
                        value[strlen(value)-1] = '\0';
                    TH_SET_USER_PASSWORD(value);
                }
#endif
#ifdef TH_MHDCAN_HTTPS
                if(strstr(option,"https"))
                {
                    parseRemoveChar(value," \n\r");
                    if(strstr(value,"true"))
                    {
                        if((FLAG & HSSL) <= 0)
                            {FLAG += HSSL;}
                    }
                }
                if(strstr(option,"sslkey"))
                {
                    //Load key
                    G_key = getUsrConfigFile(usrHomeDir,value);
                }
                if(strstr(option,"sslcert"))
                {
                    //Load cert
                    G_cert = getUsrConfigFile(usrHomeDir,value);
                }
#endif
            }

        }
        fclose(usrConfig);
    }
}

#ifdef TH_FEATURE_CLOCK
static void sigPrintIp(int signum)
{
    if(interruptTime > (time(NULL)-5))
        sigHandler(signum);
    else
    {
        printf("\n");
        interruptTime = time(NULL);
#ifdef TH_MHDCAN_HTTPS
        if ((FLAG & HSSL) > 0)
            getLocalIP("https://");
        else
#endif
            getLocalIP("http://");
#ifdef TH_FEATURE_UPNP
        IGD_printAddress();
#endif
        printf("\nTo exit: SIGINT twice within 5 seconds or SIGTERM\n");
    }
}
#endif

#ifdef USE_NORETURN
static noreturn void exit_timeout()
#else
static void exit_timeout()
#endif
{
    while(timeout != 0)
    {
#ifdef _WIN32
        Sleep(1000);
#else
        usleep(1000000);
#endif
        timeout --;
    }
    sigHandler(SIGINT);
}

int main(int argc, char **argv)
{

#ifdef TH_FEATURE_PIPE
//Check if this is a piped session
    if(!isatty(fileno(stdin)))
    {
        MODE = 5;
        char buffer[BUFSIZ];
        while(fgets(buffer,BUFSIZ,stdin))
        {
            if(txt)
            {
                txt = realloc(txt,strlen(txt)+strlen(buffer)+1);
                strncat(txt,buffer,strlen(txt)+strlen(buffer)+1);
            }
            else
            {
                txt = malloc(strlen(buffer)+1);
                strncpy(txt,buffer,strlen(buffer)+1);
            }
        }
    }
#endif
    int c;
    while (1)
    {
      int option_index = 0;

      c = getopt_long (argc, argv, "u:mlfp:sSPdRTDc:",
                       long_options, &option_index);

      if (c == -1)
        break;

      switch (c)
        {
        case 0:

            if (long_options[option_index].flag != 0)
                break;
            printf ("option %s", long_options[option_index].name);
            if (optarg)
                printf (" with arg %s", optarg);
            printf ("\n");
        break;

        case 'p':
            parsePorts(optarg);
        break;

        case 'm':
            MODE = 0;
        break;

        case 'l':
            MODE = 1;
        break;

        case 'f':
            MODE = 2;
        break;

        case 'D':
            MODE = 3;
        break;

        case 'T':
#ifdef TH_FEATURE_TREE
{
            MODE = 4;
        break;
}
#else
{
        fprintf(stderr,"[ERR] Build not capable "
            "of Tree\n");
        sigHandler(SIGINT);
}
#endif
        case 's':
            wMode = MHD_USE_SELECT_INTERNALLY;
        break;

        case 'S':
#ifdef TH_MHDCAN_HTTPS
{
        if((FLAG & HSSL) <= 0)
                {FLAG += HSSL;}
        break;
}
#else
{
        fprintf(stderr,"[ERR] Build not capable of HTTPS\n");
        sigHandler(SIGINT);
}
#endif

        case 'u':
        {
#if (MHD_VERSION >= 0x00090301)
{
        TH_SET_USER_PASSWORD(optarg);
}
#else
{
        fprintf(stderr,"[ERR] Build not capable of password prompts\n");
        sigHandler(SIGINT);
}
#endif
        }
        break;

        case 'c':
        {
            char * ptr;
            timeout = strtoul(optarg,&ptr,10) * 60;
        }
        break;

        case 'd':
            FLAG += FFDL;
            TH_CON_DIS = "attachment; filename=";
        break;

        case 'P':
#ifdef TH_FEATURE_UPNP
{
            FLAG += UPNP;
}
#else
{
        fprintf(stderr,"[ERR] Build not capable of port forwarding\n");
        sigHandler(SIGINT);
}
#endif
        break;

        case 'R':
            FLAG += RECR;
        break;

        default:
            abort ();
        }
    }

    if (optind < argc)
    {
        if(optind + 1 < argc)
        {
            MODE = 6;
            while (optind < argc)
                build_list_struct(argv[optind++]);
        }
        else
            txt = argv[optind++];
    }

    if (txt == NULL)
        nulhandler();

#if (TH_FEATURE_CLOCK == 1)
    interruptTime = time(NULL);
    signal(SIGINT,  sigPrintIp);    //Print ip on ^C, Nice close on ^C^C
#else
    signal(SIGINT,  sigHandler);    //Nice close on ^C
#endif
    signal(SIGTERM, sigHandler);    //Nice close on termination request
#ifndef _WIN32
    signal(SIGPIPE, ignore_sigpipe);//Don't close on SIGPIPE
    signal(SIGHUP,  sigHandler);    //Nice close when terminal hangup
#else
    signal(SIGBREAK,sigHandler);    //Nice close when CMD is closed.
#endif

    getUsrConfig();
    setMode();

#ifdef TH_FEATURE_INTBIND
{
    if (PORT == 0)
        PORT = randPort();
}
#endif

    printf("Starting Server... ");
    fflush(stdout);

    if(pArr)
        for(unsigned short i = 0; i < pit; i++)
        {
            PORT = pArr[i];
            startServer();
            if(wdaemon != NULL)
                break;
#ifdef _WIN32
            if(wdaemon == NULL && i == pit)
                break;
#else
            if(wdaemon == NULL)
            {
                if (errno == EADDRINUSE || errno == EACCES)
                    continue;
                else
                    break;
            }
#endif
        }
    else
        startServer();

    if (NULL == wdaemon)
    {
#ifdef _WIN32
        printf("Failed\n");
#else
        printf("Failed (%s)\n",strerror(errno));
#endif
        sigHandler(SIGINT);
    }
    else
    {
        printf("Done\n");
    }
    if(pArr){free(pArr);pArr=NULL;}

#ifndef TH_FEATURE_INTBIND
{
    if (PORT == 0)
        PORT = getServerPort(wdaemon);
}
#endif

#ifdef TH_FEATURE_UPNP
    if(((FLAG & UPNP) > 0) && PORT != 0)
    {
        printf("Attempting to port forward... ");
        fflush(stdout);
        int e = IGD_connect();
        if (e == 0)
        {
            e = IGD_addPort();
            if (e == 0)
                printf("Done\n");
            else
                printf("Error while forwarding %d (%s)\n",
                    e,strupnperror(e));
        }
        else
        {
            printf("Error while discovering %d (%s)\n",
            e,strupnperror(e));
        }
        IGD_printAddress();
    }
#endif

#ifdef TH_MHDCAN_HTTPS
    if ((FLAG & HSSL) > 0)
        getLocalIP("https://");
    else
        getLocalIP("http://");
#else
    getLocalIP("http://");
#endif

    if(timeout != 0)
    {
        printf(
            "The server will automatically shutdown in %lu seconds\n"
            ,timeout);
        exit_timeout();
    }
    else do
    {
#ifdef _WIN32
        Sleep(1000);
#else
        usleep(1000000);
#endif
    } while(1);
}
