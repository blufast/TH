/* upnp.h
 *
 * Copyright (C) 2018 Jason Lethbridge
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <miniupnpc/miniupnpc.h>
#include <miniupnpc/upnpcommands.h>
#include <miniupnpc/upnperrors.h>

#ifdef _WIN32
    #define snprintf _snprintf
#else
    #include <netinet/in.h>
#endif

extern char wan_address[64];
extern unsigned short PORT;
extern int IGD_connect(void);
extern int IGD_addPort(void);
extern int IGD_removePort(void);
extern void IGD_disconnect(void);
extern void IGD_printAddress(void);
