# What is it? #
Th (temporary HTTP) is a simple non-daemon, download only HTTP file server.

The project started as a solution to solve unreliable, slow or insecure methods 
of file transfer over the developers LAN network but can also be used over 
the internet and provides optional UPNP port forwarding.

Unlike most file hosting solutions th is not designed to run as an always 
available background daemon nor does it use any system credentials to 
verify clients. Instead a user starts th with a message, file(s) or folder(s) 
that they wish to share as parameters which that instance of the program will 
serve to any HTTP-1.1 compatible client until the program is closed. 
Since the default behaviour is to start on a ephemeral port and the 
instance can be stopped by the user with a regular signal like SIGINT (^C) 
or SIGTERM. It makes the exposure of files over the network minimal.

th supports HEAD requests, byte ranges and last modified. Which should make 
transfers over a slow or unreliable networks less frustrating if the client 
also supports and uses these features.

# Usage examples #
While you can specify the mode you want th to start in it is often unnecessary. 
th will figure out what mode to start in by the parameters given to it.

A small string can be hosted by simply typing it in as an argument

`th "Hello World"`

A file can be hosted the same way. Do note however that a misspell path will 
cause the server will treat it as a message, check to make sure the server 
started in the correct mode.

`th /path/to/host`

We can host multiple files and/or folders at the same time

`th /path/to/host/a /path/to/host/b`

By default th will not show sub-folders within a hosted folder, however you can 
change this behaviour with the --recursive (-R) flag which will allow clients 
access to all sub-folders.

`th -R /path/to/host`

In some situations it might be useful to have a single page with a big list of 
files in a folders. The opt-in build option --tree (-T) will generate a list of 
all files and sub-folders within a the path specified. Do note that when in this 
mode the list is generated at startup and changes to files being added, 
removed or renamed will not be reflected unless you restart the server.

`th -T /path/to/folder/to/host`

It is possible to specify the port number(s) you want to use with the 
--port (-p) option. Any port can be used so long as it's not occupied and the 
user has permission to use that port. When multiple ports are specified th will 
try the ports from left to right and choose the first available one.

`th -p 1234 "Hello World"`

`th -p 80,8080-8090 "Hello World"`
