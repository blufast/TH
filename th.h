/* th.h
 *
 * Copyright (C) 2018 Jason Lethbridge
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#ifdef USE_NORETURN
#include <stdnoreturn.h>
#endif
#include <string.h>
#include <unistd.h>
#include <stdarg.h>
#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <stddef.h>
#include <pthread.h>
#include <sys/time.h>
#include <time.h>
#include <getopt.h>
#include <inttypes.h>
#include <ctype.h>

#ifdef _WIN32
    #include <winsock2.h>
    #include <windows.h>    //sleep()
    #include <iphlpapi.h>   //pAdapter->
    #define MALLOC(x) HeapAlloc(GetProcessHeap(), 0, (x))
    #define FREE(x) HeapFree(GetProcessHeap(), 0, (x))
    #define uint uint_fast64_t
    #define realpath(__name,__resolved) \
    _fullpath(__resolved,__name,PATH_MAX)
#else
    #include <sys/msg.h>
    #include <sys/mman.h>
    #include <netinet/in.h>
    #include <netdb.h>
    #include <ifaddrs.h>
    #include <arpa/inet.h>
    #include <sys/select.h>
    #include <sys/socket.h>
#endif

#include <sys/stat.h>
#include <sys/types.h>

#include <microhttpd.h>

#ifndef MHD_socket
#define MHD_socket int
#endif

#include <math.h>
#include <dirent.h>

#if (MHD_VERSION >= 0x00094202 && MHD_VERSION <= 0x00095200)
#define MHD_FEATURE_MESSAGES MHD_FEATURE_MESSGES
#endif

#ifndef MHD_HTTP_HEADER_CONTENT_DISPOSITION
    #define MHD_HTTP_HEADER_CONTENT_DISPOSITION "Content-Disposition"
#endif

#ifndef MHD_HTTP_RANGE_NOT_SATISFIABLE
    #define MHD_HTTP_RANGE_NOT_SATISFIABLE \
    MHD_HTTP_REQUESTED_RANGE_NOT_SATISFIABLE
#endif

//Define macro for method of accessing and serving files
#ifndef TH_USE_FD_OPEN
    #define TH_FILE_SERVE_OFFSET(delta, file, start) \
    fseek(file, (long) start, SEEK_SET); \
    response = MHD_create_response_from_callback \
        (delta, TH_FILE_BUFFER,&file_reader,file,&file_free_callback);
#elif (TH_MHDCAN_FD64 == 1)
    #define TH_FILE_SERVE_OFFSET \
        response = MHD_create_response_from_fd_at_offset64
#else
    #define TH_FILE_SERVE_OFFSET \
        response = MHD_create_response_from_fd_at_offset
#endif

#ifndef TH_USE_FD_OPEN
    #define TH_FILE_SERVE(size,file) \
            rewind(fd); \
            response = MHD_create_response_from_callback \
                ((unsigned long long) size, 32 * 1024,&file_reader \
                ,file,&file_free_callback);
#else
    #define TH_FILE_SERVE(size,file) \
            response = MHD_create_response_from_fd(size, file);
#endif

#ifndef fseeko64
    #define fseeko64 fseeko
#endif

#ifndef ftello64
    #define ftello64 ftello
#endif

#define MIMETYPE "application/octet-stream" //Force download (-d)

//#if (MHD_VERSION > 0x00090E00)
	#define TH_GET_CLIENT_IP                \
    char ip[INET6_ADDRSTRLEN];              \
    char * ipp = getClientIp(connection);   \
    strcpy(ip,ipp);                         \
    free(ipp);                              \
    ipp = NULL
/*#else
    #define TH_GET_CLIENT_IP \
        char ip[2] = "?"
#endif*/

#if (MHD_VERSION < 0x00090400)
    #define MHD_create_response_from_buffer \
            MHD_create_response_from_data
    #define MHD_RESPMEM_MUST_FREE 1,0
    #define MHD_RESPMEM_MUST_COPY 0,1
    #define MHD_RESPMEM_PERSISTENT 0,0
    #define MHD_queue_basic_auth_fail_response(a,b,c) MHD_YES
#endif

//Inline macro to send no-cache responses whereever needed
#define TH_HTTP_HEADER_COMMON \
MHD_add_response_header(response,MHD_HTTP_HEADER_ALLOW,"GET, HEAD");\
MHD_add_response_header(response, MHD_HTTP_HEADER_CACHE_CONTROL,\
    "no-cache, private, must-revalidate");\
MHD_add_response_header(response, MHD_HTTP_HEADER_PRAGMA,\
    "no-cache");\
MHD_add_response_header(response, MHD_HTTP_HEADER_EXPIRES,\
    "0")

//Inline macro to reject requests that are anything but GET
#define TH_REJECT_IF_CLIENT_NOT_COMPLIANT \
    if (!((strcmp (method, "GET") == 0)\
    || (strcmp (method, "HEAD") == 0)))\
    {\
        fprintf(stdout,"[405] %s (%s:%s)\n",ip,method,url);\
        response = MHD_create_response_from_buffer\
            (0,"",MHD_RESPMEM_PERSISTENT);\
        MHD_add_response_header\
                (response,MHD_HTTP_HEADER_ALLOW,"GET, HEAD");\
        MHD_add_response_header\
            (response, MHD_HTTP_HEADER_CONNECTION, "close");\
        int r = MHD_queue_response\
            (connection,MHD_HTTP_METHOD_NOT_ALLOWED,response);\
        MHD_destroy_response(response);\
        return r;\
    }\
    if (NULL == *con_cls)\
    {\
        uint_fast64_t s = strlen(url)+strlen(method);\
        char * cb_url = calloc(sizeof(char),s+2);\
        snprintf(cb_url,s+2,"%s:%s",method,url);\
        *con_cls = cb_url;\
        return MHD_YES;\
    }\
\
    int r = credOk(connection,ip);\
\
    if (r != 0)\
    {\
        TH_HTTP_CODE_PRNT(MHD_HTTP_UNAUTHORIZED);\
        TH_HTTP_CODE_POST(MHD_HTTP_UNAUTHORIZED);\
        r = MHD_queue_basic_auth_fail_response\
            (connection,"This page requires authentication",response);\
        MHD_destroy_response(response);\
        return r;\
    }

//Get users directory for discovering local config directory
#ifdef _WIN32
#define TH_STR_ENVDIR "APPDATA"
#else
#define TH_STR_ENVDIR "HOME"
#endif

//Help instructions for when the program runs without parameters
static const char * TH_STR_CMD_HELP =
"th [OPTIONS] [ MESSAGE | DIRECTORY ]\n\n"
"   -m --message\n"
"   -f --file\n"
"   -l --link\n"
"   -D --path\n"
#ifdef TH_FEATURE_TREE
"   -T --tree\n"
#endif
"   -R --recursive\n"
#if (MHD_VERSION >= 0x00090301)
"   -u --user\n"
#endif
"   -s --single-serve\n"
#ifdef TH_MHDCAN_HTTPS
"   -S --https\n"
#endif
"   -p --port\n"
#ifdef TH_FEATURE_UPNP
"   -P --port-forward\n"
#endif
"   -d --download\n"
"   -c --countdown\n"
"\n";

//Static HTML header, to be used in a snprintf statement
#define TH_STR_HTML_HEAD \
"<!DOCTYPE HTML>\n<html>\n<head>\n"\
"<meta name=\"robots\" content=\"noindex, nofollow\">\n"\
"<meta http-equiv=\"Content-Type\""\
" content=\"text/html;charset=utf-8\">\n"\
"<title>%s</title>\n"

#define TH_STR_HEAD_FOOT    "</head>\n"
#define TH_STR_BODY_HEAD    "<body>\n"
#define TH_STR_HTML_FOOT    "</body>\n</html>"
#define TH_STR_HTML_STYLE   "<style>%s</style>\n"

#define TH_CSS_GLOBAL       "*{font-family:monospace;}"
#define TH_CSS_TABLE \
TH_CSS_GLOBAL \
"table{width:100%%;}"\
"td:nth-child(1){text-align:left;}"\
"td:nth-child(2){text-align:right;}"\
"td:nth-child(3){text-align:right;}"

//Static table header, used to list files neatly
#define TH_STR_TABLE_HEAD   "<table>\n"

//Static table footer, to be used at the end of HTML_HEAD
#define TH_STR_TABLE_FOOT   "</table>\n"

//Inline macro to check if response time from client is same as file
#define TH_REJECT_304_IF_DATE_OK \
if(creqdate != NULL)\
{\
    if (strcmp(mdate,creqdate) == 0)\
    {\
        TH_HTTP_CODE_POST_BLANK;\
        TH_HTTP_CODE_PRNT(MHD_HTTP_NOT_MODIFIED);\
        TH_HTTP_CODE_SEND(MHD_HTTP_NOT_MODIFIED);\
        TH_HTTP_HEADER_COMMON;\
        MHD_add_response_header\
            (response, MHD_HTTP_HEADER_LAST_MODIFIED,mdate);\
        free(mdate); mdate=NULL;\
        MHD_destroy_response (response);\
        return r;\
    }\
}

//Same as above but for dynamic code?
#define TH_DYNAMIC_REJECT_304_IF_DATE_OK \
if(creqdate != NULL)\
{\
    if (strcmp(mdate,creqdate) == 0)\
    {\
        TH_HTTP_CODE_POST_BLANK;\
        TH_HTTP_CODE_PRNT(MHD_HTTP_NOT_MODIFIED);\
        TH_HTTP_CODE_SEND(MHD_HTTP_NOT_MODIFIED);\
        TH_HTTP_HEADER_COMMON;\
        MHD_add_response_header\
            (response, MHD_HTTP_HEADER_LAST_MODIFIED,mdate);\
        free(mdate); mdate=NULL;\
        free(curdir); curdir=NULL;\
        MHD_destroy_response (response);\
        return r;\
    }\
}

//304 but for G_date, no free
#define TH_REJECT_304_IF_GDATE_OK \
if(creqdate != NULL)\
{\
    if (strcmp(G_date,creqdate) == 0)\
    {\
        TH_HTTP_CODE_POST_BLANK;\
        TH_HTTP_CODE_PRNT(MHD_HTTP_NOT_MODIFIED);\
        TH_HTTP_CODE_SEND(MHD_HTTP_NOT_MODIFIED);\
        TH_HTTP_HEADER_COMMON;\
        MHD_add_response_header\
            (response, MHD_HTTP_HEADER_LAST_MODIFIED,G_date);\
        MHD_destroy_response (response);\
        return r;\
    }\
}

//Same as above but for dynamic code
#define TH_DYNAMIC_REJECT_304_IF_GDATE_OK \
if(creqdate != NULL)\
{\
    if (strcmp(G_date,creqdate) == 0)\
    {\
        TH_HTTP_CODE_POST_BLANK;\
        TH_HTTP_CODE_PRNT(MHD_HTTP_NOT_MODIFIED);\
        TH_HTTP_CODE_SEND(MHD_HTTP_NOT_MODIFIED);\
        TH_HTTP_HEADER_COMMON;\
        MHD_add_response_header\
            (response, MHD_HTTP_HEADER_LAST_MODIFIED,G_date);\
        free(curdir); curdir=NULL;\
        MHD_destroy_response (response);\
        return r;\
    }\
}

//Inline Macros to handle method of opening files determined by config
#ifdef TH_USE_FD_OPEN
    #define fsize sbuf.st_size  //fsize == sbuf.st_size
    #define TH_IF_FILE_OPEN(MFILE) \
        if ((-1 == (fd = open (MFILE, O_RDONLY))) || \
            (0 != fstat (fd, &sbuf)))\


#else
    #define TH_IF_FILE_OPEN(MFILE) \
        FILE * fd = fopen(MFILE,"rb");\
        long long fsize = 0;\
        if ((0 != fseeko64(fd, 0, SEEK_END)) ||\
                (-1 == (fsize = (long long) ftello64(fd))))
#endif

//Inline macro to serve a file in modes that can do other things
#define TH_DYNAMIC_FILE(MFILE ,MSTAT) \
    if(canRead(MFILE) == 0) \
{ \
    free(MFILE);MFILE = NULL;\
    TH_HTTP_CODE_PRNT(MHD_HTTP_FORBIDDEN);\
    TH_HTTP_CODE_POST(MHD_HTTP_FORBIDDEN);\
    TH_HTTP_CODE_SEND(MHD_HTTP_FORBIDDEN);\
    MHD_destroy_response(response);\
    return MHD_YES;\
}\
    TH_IF_FILE_OPEN(MFILE) \
{ \
    free(MFILE);MFILE = NULL;\
    TH_HTTP_CODE_PRNT(MHD_HTTP_NOT_FOUND);\
    TH_HTTP_CODE_POST(MHD_HTTP_NOT_FOUND);\
    TH_HTTP_CODE_SEND(MHD_HTTP_NOT_FOUND);\
    MHD_destroy_response(response);\
    return MHD_YES;\
}\
if(fsize == 0)\
{\
    TH_HTTP_CODE_PRNT(MHD_HTTP_NO_CONTENT);\
    TH_HTTP_CODE_POST_BLANK;\
    TH_HTTP_CODE_SEND(MHD_HTTP_NO_CONTENT);\
    MHD_destroy_response (response);\
    free(MFILE);MFILE=NULL;\
    return r;\
}\
\
char * mdate = parseTime(MSTAT.st_mtime);\
\
const char * creqdate =\
MHD_lookup_connection_value\
    (connection,MHD_HEADER_KIND,\
    MHD_HTTP_HEADER_IF_MODIFIED_SINCE);\
\
TH_DYNAMIC_REJECT_304_IF_DATE_OK;\
\
const char * Range =\
MHD_lookup_connection_value\
    (connection,MHD_HEADER_KIND,MHD_HTTP_HEADER_RANGE);\
\
if(Range != NULL)\
{\
    struct byteOffset bytes = parseOffset\
        (Range,(unsigned long long) fsize);\
\
    if(bytes.ok == 1)\
    {\
        TH_HTTP_CODE_PRNT(MHD_HTTP_PARTIAL_CONTENT);\
        TH_FILE_SERVE_OFFSET\
            (bytes.offset_Delta, fd, bytes.offset_Start);\
        TH_HTTP_CODE_SEND(MHD_HTTP_PARTIAL_CONTENT);\
    }\
    else\
    {\
        TH_HTTP_CODE_POST_BLANK;\
        TH_HTTP_CODE_PRNT(MHD_HTTP_RANGE_NOT_SATISFIABLE);\
        TH_HTTP_CODE_SEND(MHD_HTTP_RANGE_NOT_SATISFIABLE);\
    }\
    MHD_add_response_header\
        (response,MHD_HTTP_HEADER_CONTENT_RANGE,\
        bytes.header);\
}\
else\
{\
    TH_FILE_SERVE(fsize,fd);\
}\
\
char filename[FILENAME_MAX];\
char * t = getFileName(MFILE);\
strncpy(filename,t,FILENAME_MAX);\
free(t); t=NULL;\
MHD_add_response_header\
    (response,MHD_HTTP_HEADER_CONTENT_DISPOSITION,filename);\
MHD_add_response_header\
    (response,MHD_HTTP_HEADER_ACCEPT_RANGES,"bytes");\
MHD_add_response_header\
    (response,MHD_HTTP_HEADER_LAST_MODIFIED,mdate);\
TH_HTTP_HEADER_COMMON;\
if((FLAG & FFDL) > 0)\
{\
    MHD_add_response_header\
        (response,MHD_HTTP_HEADER_CONTENT_TYPE,MIMETYPE);\
}\
if (Range == NULL)\
{\
    TH_HTTP_CODE_PRNT(MHD_HTTP_OK);\
    TH_HTTP_CODE_SEND(MHD_HTTP_OK);\
}\
free(mdate);mdate=NULL;\
free(MFILE);MFILE=NULL;\
MHD_destroy_response (response);\
return r

//Inline Macros to handle method of dirent listing defined by config
#ifdef TH_FEATURE_SORT

#define TH_DIRENT_INIT(dir) \
struct dirent *ep;\
struct dirent **sortep;\
uint dit = 0;\
int n = scandir(dir, &sortep, NULL, alphasort);\
if (n < 0)\
perror("scandir");\
else \
while (dit < n)

#define TH_DIRENT_CLEANUP_ITERATION() \
ep = NULL;\
free(sortep[dit]);\
sortep[dit] = NULL;\
dit++;

#define TH_DIRENT_CLEANUP() \
free(sortep);

#else

#define TH_DIRENT_INIT(dir) \
DIR * dp = NULL;\
struct dirent *ep = NULL;\
if((dp = opendir(dir)) == NULL)\
{\
printf("Failed (%s)\n",strerror(errno));\
sigHandler(SIGINT);\
}\
if (dp != NULL)\
while ((ep = readdir(dp)) != NULL)

#define TH_DIRENT_CLEANUP_ITERATION()

#define TH_DIRENT_CLEANUP()\
if(dp) \
{(void) closedir(dp);} \
if(ep) \
{free(ep); \
ep=NULL;}

#endif

//Inline macro for setting user password
#define TH_SET_USER_PASSWORD(str) \
    char * t_user; char * t_pass;\
    t_user = strtok(str, " ");\
    t_pass = strtok(NULL, " ");\
    if(t_pass == NULL)\
    {\
        t_pass = t_user;\
    }\
    user = calloc(strlen(t_user)+1,sizeof(char));\
    pass = calloc(strlen(t_pass)+1,sizeof(char));\
    strncpy(user,t_user,strlen(t_user));\
    strncpy(pass,t_pass,strlen(t_pass));\
    FLAG += CRED

//Function macro for determining if port is within range
#define TH_CHECK_PORT_VALID(charport,tmp_port,port) \
    tmp_port = atoi(charport);\
    if ((tmp_port < 0) || (tmp_port > 65534))\
    {\
        printf("[ERR] Port '%s' is not within valid range (1-65534)\n"\
        ,charport);\
        sigHandler(SIGINT);\
    }\
    else \
    {\
        port = (unsigned short)tmp_port;\
    }

#define TH_MODE_PRINT(TH_MODE) \
printf(#TH_MODE ": %s\n",(char *) txt)

//Disposition header
static char * TH_CON_DIS = "filename=";
//Static filename in filemode
static char TH_STATIC_FILENAME[FILENAME_MAX];

static struct MHD_OptionItem * G_options = NULL; //MHD_OPTIONS

//Define these variables only when HTTPS is built
#ifdef TH_MHDCAN_HTTPS
static char * G_cert = NULL;
static char * G_key  = NULL;
static char * getUsrConfigFile(char * usrHomeDir,char * value);
#endif

static int verbose_flag;

static unsigned int wMode = MHD_USE_THREAD_PER_CONNECTION |
#if (MHD_VERSION >= 0x00095206)
    MHD_USE_INTERNAL_POLLING_THREAD;
#else
	MHD_USE_SELECT_INTERNALLY;
#endif

static void sigHandler(int);
static struct MHD_Daemon *wdaemon;

//Users argument, could be a message or directory
static void * txt = NULL;

static int_fast8_t  MODE = -1;  //Unknown by default
static uint_fast8_t     FLAG = 0;

enum FLAGTYPE //Options byteflag
{
    CRED = (1<<1),  //Credenicals required
    FFDL = (1<<2),  //Force file download
    UPNP = (1<<3),  //Attempt port forwarding to wan
    RECR = (1<<4),  //Recursive in Path mode
    HSSL = (1<<5)   //Start in MHD with HTTPS enabled
};

extern unsigned short PORT;

//Define these variables only when the build of MHD can support it
#if (MHD_VERSION >= 0x00090301)
static char * user;
static char * pass;
#endif

static unsigned long timeout = 0; //Off by default
#ifdef TH_FEATURE_CLOCK
static time_t   interruptTime = 0; //Last interrupt time
#endif

static char * G_page;   //Global page           for file
static char * G_date;   //Global date of page if it's data is constant

struct ltxt
{
    char * path;
    char * adir;
    struct ltxt * nxt;
};

struct byteOffset
{
    unsigned long long offset_Start;    //Beginning for byte range
    unsigned long long offset_End;      //End of byte range
    unsigned long long offset_Delta;    //Legnth of byte range
    char header[128];                   //http Range header
    short ok;                           //0 for okay else for error
};

#ifdef TH_FEATURE_UPNP
    #include "upnp.h"
#endif

static struct option long_options[] =
{
    {"verbose"          ,no_argument    ,&verbose_flag, 1},
    {"brief"            ,no_argument    ,&verbose_flag, 0},

    {"message"          ,no_argument        ,0,'m'},
    {"link"             ,no_argument        ,0,'l'},
    {"list"             ,no_argument        ,0,'L'},
    {"tree"             ,no_argument        ,0,'t'},
    {"file"             ,no_argument        ,0,'f'},
    {"port"             ,required_argument  ,0,'p'},
    {"user"             ,required_argument  ,0,'u'},
    {"single-serve"     ,no_argument        ,0,'s'},
    {"countdown"        ,required_argument  ,0,'c'},
    {"download"         ,no_argument        ,0,'d'},
    {"port-forward"     ,no_argument        ,0,'P'},
    {"recursive"        ,no_argument        ,0,'R'},
    {0, 0, 0, 0}
};

//Deal with variable type change between different versions of MHD
#ifndef TH_USE_FD_OPEN
static unsigned int TH_FILE_BUFFER = 32 * 1024;
#if (MHD_VERSION >= 0x00090000)
static ssize_t file_reader
    (void *cls, uint64_t pos, char *buf, size_t max)
#else
static int file_reader (void *cls, uint64_t pos, char *buf, int max)
#endif
{
  FILE *file = cls;
  return (ssize_t) fread (buf, 1, max, file);
}


static void file_free_callback (void *cls)
{
  FILE *file = cls;
  fclose (file);
}
#endif

//Only print these messages in a debug build
/*#ifndef NDEBUG
#define DEBUGPRT(msg,arg) \
printf("DEBUG: "msg,arg);
#else
#define DEBUGPRT(msg,arg)
#endif*/

//Print code in console
#define TH_HTTP_CODE_PRNT(NUM) \
printf("[%c%d] %s%s\n",method[0],NUM,ip,url); \

//Send code as the containers actual data on the clients screen;
#if (MHD_VERSION >= 0x00094801)
#define TH_HTTP_CODE_POST(NUM) \
char M_p[64];\
strncpy(M_p,MHD_get_reason_phrase_for(NUM),64);\
response = MHD_create_response_from_buffer\
(strlen(M_p),(void*)M_p,MHD_RESPMEM_MUST_COPY);
#else
#define TH_HTTP_CODE_POST(NUM) \
char * M_p;\
M_p = malloc(10);\
snprintf(M_p,10,"HTTP: %d",NUM);\
response = MHD_create_response_from_buffer\
(strlen(M_p),M_p,MHD_RESPMEM_MUST_FREE);
#endif

#define TH_HTTP_CODE_POST_BLANK \
response = MHD_create_response_from_buffer(0,"",MHD_RESPMEM_PERSISTENT);

//Set code response
#define TH_HTTP_CODE_SEND(NUM) \
r = MHD_queue_response(connection, NUM, response);
