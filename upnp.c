/* upnp.c
 *
 * Copyright (C) 2018 Jason Lethbridge
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include "upnp.h"

static struct UPNPDev* igdList = 0;
static struct UPNPUrls upnp_urls;
static struct IGDdatas upnp_data;
static char lan_address[64];
char wan_address[64];
static char charPort [6];

int IGD_connect() //Connect to IGD if available
{
    int err = 0; //Error interger returned from miniupnp
#if (MINIUPNPC_API_VERSION >= 14)
    igdList = upnpDiscover(2000, 0, NULL, 0, 0, 0, &err);
#else
    igdList = upnpDiscover(2000, 0, NULL, 0, 0, &err);
#endif
    UPNP_GetValidIGD(igdList, &upnp_urls, &upnp_data,
        lan_address, sizeof(lan_address));
    UPNP_GetExternalIPAddress(upnp_urls.controlURL,
        upnp_data.first.servicetype, wan_address);
    return err;
}

void IGD_disconnect() //Cleanup miniupnpc structure
{
    FreeUPNPUrls(&upnp_urls);
    freeUPNPDevlist(igdList);
    return;
}

int IGD_addPort() //Add a charPort to the connected IGD
{
    snprintf(charPort,6,"%" PRIu16 ,PORT);
    int err = UPNP_AddPortMapping(upnp_urls.controlURL ,
        upnp_data.first.servicetype ,charPort ,charPort ,lan_address ,
        "tmphttp" ,"TCP" ,"","0");
    return err;
}

int IGD_removePort() //Remove a charPort to the connected IGD
{
    int err = UPNP_DeletePortMapping(upnp_urls.controlURL,
    upnp_data.first.servicetype,charPort,"TCP",NULL);
    return err;
}

void IGD_printAddress()
{
    if (wan_address[0] != '\0')
    {
#ifdef TH_MHDCAN_HTTPS
        if ((FLAG & HSSL) > 0)
            printf("https://%s:%d (IGD: %s)\n",
                wan_address,PORT,upnp_data.presentationurl);
        else
#endif
            printf("http://%s:%d (IGD: %s)\n",
                wan_address,PORT,upnp_data.presentationurl);
    }
}

